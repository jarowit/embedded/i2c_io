add_library(system STATIC)

target_include_directories(system
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/include
        ${CMAKE_CURRENT_LIST_DIR}/include/config
        ${CMAKE_CURRENT_LIST_DIR}/include/cmsis
        ${CMAKE_CURRENT_LIST_DIR}/include/stm32f3-stdperiph
)

target_sources(system
    PRIVATE
#        src/cmsis/system_stm32f30x.c
#        src/cmsis/vectors_stm32f30x.c

        src/cortexm/_initialize_hardware.c
        src/cortexm/_reset_hardware.c
        src/cortexm/exception_handlers.c

        src/diag/Trace.c
        src/diag/trace_impl.c

        src/newlib/_exit.c
        src/newlib/_sbrk.c
        src/newlib/_startup.c
        src/newlib/_syscalls.c
        src/newlib/_cxx.cpp
        src/newlib/assert.c

        src/stm32f3-stdperiph/stm32f30x_gpio.c
        src/stm32f3-stdperiph/stm32f30x_rcc.c
)
