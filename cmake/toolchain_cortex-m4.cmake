set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR cortex-m4)

set(CPU_FLAGS "-mthumb -mcpu=cortex-m4")
include(${CMAKE_CURRENT_LIST_DIR}/compiler/arm-none-eabi.cmake)
