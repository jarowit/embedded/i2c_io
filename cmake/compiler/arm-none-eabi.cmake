set(tools /home/ltyrala/work/_soft/gcc-arm-none-eabi-9-2019-q4-major/bin)

set(CMAKE_ASM_COMPILER ${tools}/arm-none-eabi-gcc)
set(CMAKE_C_COMPILER ${tools}/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER ${tools}/arm-none-eabi-g++)
set(AS ${tools}/arm-none-eabi-as)
set(AR ${tools}/arm-none-eabi-ar)
set(OBJCOPY ${tools}/arm-none-eabi-objcopy)
set(OBJDUMP ${tools}/arm-none-eabi-objdump)
set(SIZE ${tools}/arm-none-eabi-size)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_COMPILER_WORKS TRUE)
set(CMAKE_CXX_COMPILER_WORKS TRUE)

string(CONCAT c_flags
        # " -g -ggdb3"
        " -g3"
        " -Wall -Wextra -Wpedantic"
        " -fmessage-length=0"
        " -fsigned-char"
        " -ffunction-sections"
        " -fdata-sections"
        " -ffreestanding"
#        " -fno-builtin"
)

string(CONCAT cxx_flags
        "${c_flags}"
        " -fabi-version=0"
        " -fno-exceptions"
        " -fno-rtti"
        " -fno-use-cxa-atexit"
        " -fno-threadsafe-statics"
)

string(CONCAT link_flags
        " --specs=nano.specs"
        " --specs=nosys.specs"
        " -Xlinker --gc-sections"
        " -nostartfiles"
)

set(CMAKE_C_FLAGS   "${CPU_FLAGS} ${c_flags}" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS "${CPU_FLAGS} ${cxx_flags}" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS "${CPU_FLAGS} -x assembler-with-cpp" CACHE INTERNAL "asm compiler flags")
set(CMAKE_EXE_LINKER_FLAGS "${link_flags}" CACHE INTERNAL "exe link flags")

SET(CMAKE_C_FLAGS_DEBUG   "-Og -fno-move-loop-invariants" CACHE INTERNAL "c debug compiler flags")
SET(CMAKE_CXX_FLAGS_DEBUG "-Og -fno-move-loop-invariants" CACHE INTERNAL "cxx debug compiler flags")
SET(CMAKE_ASM_FLAGS_DEBUG ""    CACHE INTERNAL "asm debug compiler flags")

SET(CMAKE_C_FLAGS_RELEASE   "-Os" CACHE INTERNAL "c release compiler flags")
SET(CMAKE_CXX_FLAGS_RELEASE "-Os" CACHE INTERNAL "cxx release compiler flags")
SET(CMAKE_ASM_FLAGS_RELEASE ""    CACHE INTERNAL "asm release compiler flags")
